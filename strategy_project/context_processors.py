from django.conf import settings

def application_metadata(request):
    return {'APPLICATION_NAME': settings.APPLICATION_NAME,
    'APPLICATION_VERSION': settings.APPLICATION_VERSION}