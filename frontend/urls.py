from django.urls import path

from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.index, name='home'),
    path('', views.index, name='game_list'),
    path('', views.index, name='user_profile'),
    path('login', auth_views.LoginView, {'next_page': '/'}, name='login'),
    path('logout', auth_views.LogoutView, {'next_page': '/'}, name='logout'),
]
